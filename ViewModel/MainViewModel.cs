﻿using CredytyClient.Entities;
using CredytyClient.Model;
using CredytyClient.Utilities;
using CredytyClient.ViewModel.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace CredytyClient.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        private bool canExecute = true;
        private int userId;        

        public int UserId
        {
            get { return userId; }
            set { userId = value;
                OnPropertyChanged("UserId");
            }
        }

        private string name;

        public string Name
        {
            get { return name; }
            set { name = value;
                OnPropertyChanged("Name");
            }
        }

        private string email;

        public string Email
        {
            get { return email; }
            set { email = value;
                OnPropertyChanged("Email");
            }
        }

        private string password;

        public string Password
        {
            get { return password; }
            set { 
                password = value;
                OnPropertyChanged("Password");
            }
        }

        private ObservableCollection<User> userList;

        public ObservableCollection<User> UserList  
        {
            get { return userList; }
            set { userList = value;
                OnPropertyChanged("UserList");
            }
        }


        private ICommand getUsers;

        public ICommand GetUsers
        {
            get { 

                if(getUsers == null)
                {
                    getUsers = new RelayCommand(LoadUsers, param => this.canExecute);
                }
                return getUsers; 
            }            
        }

        private void LoadUsers(object obj)
        {
            UserList = UserModel.Instance.GetUsers();
        }

        private void CanExecuteCommand(object obj)
        {
            
        }
    }
}
