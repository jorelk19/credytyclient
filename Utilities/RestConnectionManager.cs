﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CredytyClient.Utilities
{
    public class RestConnectionManager
    {
        /// <summary>
        /// Private var to get the instance to manage the singleton
        /// </summary>
        private static RestConnectionManager instance;

        /// <summary>
        /// Instance to manage the singleton for the class
        /// </summary>
        public static RestConnectionManager Instance
        {
            get
            {
                if (instance != null)
                {
                    return instance;
                }
                instance = new RestConnectionManager();
                return instance;
            }
        }

        public T SendService<T>(string url, string endpoint, Method method, object jsonParam)
        {
            try
            {
                var client = new RestClient(url);
                var request = new RestRequest(endpoint, method);
                request.AddJsonBody(jsonParam);
                IRestResponse<T> response = client.Execute<T>(request);
                return response.Data;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            var result = Activator.CreateInstance<T>();
            return result;
        }

        public T GetService<T>(string url, string endpoint, Method method)
        {
            try
            {
                var client = new RestClient(url);
                var request = new RestRequest(endpoint, method);
                IRestResponse<T> response = client.Execute<T>(request);
                return response.Data;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            var result = Activator.CreateInstance<T>();
            return result;
        }
    }
}
