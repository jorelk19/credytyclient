﻿using CredytyClient.Entities;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CredytyClient.Utilities
{
    public class ServiceManager
    {
        private static ServiceManager instance;

        public static ServiceManager Instance
        {
            get {
                if (instance == null)
                {
                    instance = new ServiceManager();
                }
                return instance; 
            }            
        }

        public List<User> GetAllUsers()
        {
            var url = ConfigurationManager.AppSettings.Get("integrationApi");
            return RestConnectionManager.Instance.GetService<List<User>>(url, "/getusers", Method.GET);
        }

    }
}
