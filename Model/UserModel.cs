﻿using CredytyClient.Entities;
using CredytyClient.Utilities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CredytyClient.Model
{
    class UserModel
    {
        private static UserModel instance;

        public static UserModel Instance
        {
            get {
                if (instance == null)
                {
                    instance = new UserModel();
                }
                return instance;
            }        
        }


        public ObservableCollection<User> GetUsers()
        {
            return new ObservableCollection<User>(ServiceManager.Instance.GetAllUsers());
        }

    }
}
